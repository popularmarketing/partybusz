-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Ápr 17. 14:47
-- Kiszolgáló verziója: 5.6.29
-- PHP verzió: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Partybusz', '105e2-logo1.png', '6f74b-logo.png', '+36 (30) 301 5675', '', '', '', '', 'Budapest, 1146', '<p>\r\n	Mind&iacute;g nyitva vagyunk!!</p>\r\n', '', 'partybus@partybus.co.hu', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `galeria`
--

INSERT INTO `galeria` (`id`, `image_url`, `file`, `nev`, `active`) VALUES
(1, '', '2100c-20-fos-partybusz.jpg', 'partybusz', 1),
(2, '', '20300-partybushatter.jpg', 'partybusz', 0),
(3, '', '3002e-partybussightseeing.jpg', 'partybusz', 0),
(4, '', 'e9d8d-partybusz14.jpg', 'partybusz', 0),
(5, '', '18b32-partybusz-50.jpg', 'partybusz', 0),
(6, '', '312f6-_mg_2185.jpg', 'limuzin', 1),
(7, '', '7a1b9-62hummerlimo15.jpg', 'limuzin', 0),
(8, '', '2f81b-592004belso3.jpg', 'limuzin', 0),
(9, '', '9b7ac-hummer-1.jpg', 'limuzin', 0),
(10, '', '98bc2-hummer101.jpg', 'limuzin', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `kategoriak`
--

INSERT INTO `kategoriak` (`id`, `nev`, `url`, `szulo`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(3, 'PARTYBUSZ', 'partybusz', '', 0, 1, '', '', '', '', '', ''),
(4, 'LIMUZIN', 'limuzin', '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(52, 'Főoldal', 'fooldal', '<h1 class="rock_welcome">\r\n	&Uuml;dv&ouml;z&ouml;lj&uuml;k a Partybusz honlapj&aacute;n!</h1>\r\n<p>\r\n	Itt megtal&aacute;lhatsz minden inform&aacute;ci&oacute;t nem h&eacute;tk&ouml;znapi partybuszos bulijainkr&oacute;l, &aacute;rainkr&oacute;l, szolg&aacute;ltat&aacute;sainkr&oacute;l, illetve extra szolg&aacute;ltat&aacute;sainkr&oacute;l. &Uuml;nnepelj vel&uuml;nk, &eacute;s lepd meg szeretteid, bar&aacute;taid egy rendk&iacute;v&uuml;li partybuszoz&aacute;ssal! Sz&uuml;linap vagy leg&eacute;nyb&uacute;cs&uacute;, v&aacute;rosn&eacute;z&eacute;s vagy kocsmat&uacute;ra, c&eacute;ges buli vagy oszt&aacute;lytal&aacute;lkoz&oacute; &ndash; mi mindegyik alkalmat felejthetetlenn&eacute; var&aacute;zsoljuk!</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(53, 'Partybuszok', 'partybuszok', '<h1>\r\n	Partybuszok</h1>\r\n<p>\r\n	<strong>Sz&uuml;let&eacute;snapra, szalagavat&oacute;ra, leg&eacute;nyb&uacute;cs&uacute;ra, le&aacute;nyb&uacute;cs&uacute;ra, illetve b&aacute;rmilyen m&aacute;s alkalomra vagy ha egyszerűen csak bulizni akarsz, egy eddig m&eacute;g nem l&aacute;tott, egyed&uuml;l&aacute;ll&oacute;, ig&eacute;nyes vil&aacute;g t&aacute;rulhat el&eacute;d.</strong></p>\r\n<p>\r\n	<strong>A hangulatot fokozz&aacute;k a party f&eacute;nyek, klub hangrendszer &eacute;s a kr&oacute;m t&aacute;ncr&uacute;d, mely a t&aacute;nct&eacute;r k&ouml;zep&eacute;n tal&aacute;lhat&oacute;,<span style="vertical-align: super; line-height: 1.3em;">&nbsp;&nbsp;</span></strong></p>\r\n<table border="2" cellpadding="5" class="partybus-list">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: text-align: center;" width="25%">\r\n				<p>\r\n					<strong>20 fős Partybus (limobus)</strong></p>\r\n				<p>\r\n					<a href="partybusz-20fore.html"><img alt="" src="images/img/20-fos-partybusz.jpg" width="144" /></a></p>\r\n			</td>\r\n			<td style="border: text-align: center;" width="25%">\r\n				<p>\r\n					<strong>35 fős Partybus</strong></p>\r\n				<p>\r\n					<a href="partybusz-35fore.html"><img alt="" src="images/img/partybushatter.jpg" width="144" /></a></p>\r\n			</td>\r\n			<td style="border: text-align: center;" width="25%">\r\n				<p>\r\n					<strong>50 fős Partybus</strong></p>\r\n				<p>\r\n					<a href="partybusz-50fore.html"><img alt="" src="images/img/partybusz-50.jpg" width="144" /></a></p>\r\n			</td>\r\n			<td style="border: text-align: center;" width="25%">\r\n				<p>\r\n					<strong>70 fős Partybus&nbsp;</strong></p>\r\n				<p>\r\n					<a href="partybusz-70fore.html"><img alt="" src="images/img/partybusz14.jpg" width="144" /></a></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- 20 szem&eacute;lyes</strong><br />\r\n					<strong>- 9m hossz&uacute; (7.3 V8-as, (Magyarorsz&aacute;gon elsők&eacute;nt forgalomba helyezett t&iacute;pus!))</strong></p>\r\n			</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- 18-35 szem&eacute;lyes</strong><br />\r\n					<strong>- 11m hossz&uacute;</strong></p>\r\n			</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- 20-50 szem&eacute;lyes</strong><br />\r\n					<strong>- 15m hossz&uacute;</strong></p>\r\n			</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- 20-70 szem&eacute;lyes</strong><br />\r\n					<strong>- 18m hossz&uacute;</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Nagy t&aacute;nct&eacute;r kr&oacute;m t&aacute;ncr&uacute;ddal, 200cm belmagass&aacute;g</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Nagy t&aacute;nct&eacute;r kr&oacute;m t&aacute;ncr&uacute;ddal, 210cm belmagass&aacute;g</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Nagy t&aacute;nct&eacute;r kr&oacute;m t&aacute;ncr&uacute;ddal, 240cm belmagass&aacute;g</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 2 nagy t&aacute;nct&eacute;r 2 db kr&oacute;m t&aacute;ncr&uacute;ddal, 250cm belmagass&aacute;g</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 2 b&aacute;rpult italtart&oacute;kkal</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 200 l-es hord&oacute;, ak&aacute;r tele j&eacute;gbe hűt&ouml;tt italokkal, szemetessel</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 1 b&aacute;rpult italtart&oacute;kkal</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 2 hatalmas asztal italtart&oacute;val, szemetessel</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Limuzin design &uuml;l&eacute;ssor</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- VIP kanap&eacute;</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Limuzin design &uuml;l&eacute;ssor</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Limuzinokhoz hasonl&oacute;an f&eacute;lk&ouml;rben az &uuml;l&eacute;sek</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Party &eacute;s hangulat f&eacute;nyek (l&eacute;zerek, magic ledes tető, &eacute;s hangvez&eacute;relt ledes hangulat f&eacute;nyek)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Party &eacute;s hangulat f&eacute;nyek (l&eacute;zerek, ledes l&aacute;mp&aacute;k, stroboszk&oacute;p)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Party &eacute;s hangulat f&eacute;nyek (l&eacute;zerek, magic ledes tető, &eacute;s hangvez&eacute;relt ledes hangulat f&eacute;nyek)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Party &eacute;s hangulat f&eacute;nyek (l&eacute;zerek, ledes l&aacute;mp&aacute;k, stroboszk&oacute;p)</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>&nbsp;</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- F&uuml;stg&eacute;p</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- F&uuml;stg&eacute;p</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- F&uuml;stg&eacute;p</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 2 darab LCD LED-es 24&quot;-os monitor</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 1 hatalmas k&ouml;zponti LCD TV 82cm</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 7 darab LCD LED-es 22&Prime;-os monitor</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- 6 darab LCD LED-es 22&quot;-os monitorok</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Komoly Autohifi hangrendszer (8db nagyov&aacute;l hangszor&oacute; 2db m&eacute;lynyom&oacute;)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Komoly klub hangrendszer (1500 Watt)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Komoly szabadt&eacute;ri hangos&iacute;t&aacute;s (3000W)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Komoly klub hangrendszer (3000 Watt)</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Aut&oacute;r&aacute;di&oacute; (USB, jack bemenettel)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Sz&aacute;m&iacute;t&oacute;g&eacute;p (b&aacute;rmilyen form&aacute;tum lej&aacute;tsz&aacute;sa &eacute;rdek&eacute;ben)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Sz&aacute;m&iacute;t&oacute;g&eacute;p (b&aacute;rmilyen form&aacute;tum lej&aacute;tsz&aacute;sa &eacute;rdek&eacute;ben)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Sz&aacute;m&iacute;t&oacute;g&eacute;p (b&aacute;rmilyen form&aacute;tum lej&aacute;tsz&aacute;sa &eacute;rdek&eacute;ben)</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- MP3 form&aacute;tum lej&aacute;tsz&aacute;s pendriver&oacute;l</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- MP3 form&aacute;tum lej&aacute;tsz&aacute;s pendriver&oacute;l</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- MP3 form&aacute;tum lej&aacute;tsz&aacute;s pendriver&oacute;l</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- MP3 form&aacute;tum lej&aacute;tsz&aacute;s pendriver&oacute;l</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- Panor&aacute;m&aacute;s s&ouml;t&eacute;t&iacute;tett ablakok</strong></p>\r\n			</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Panor&aacute;m&aacute;s s&ouml;t&eacute;t&iacute;tett ablakok</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Panor&aacute;m&aacute;s s&ouml;t&eacute;t&iacute;tett ablakok</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<p>\r\n					<strong>- Panor&aacute;m&aacute;s s&ouml;t&eacute;t&iacute;tett ablakok</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- K&aacute;r&oacute;mint&aacute;s Bőr&uuml;l&eacute;sek </strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Bőr&uuml;l&eacute;sek</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- K&aacute;r&oacute;mint&aacute;s bőr&uuml;l&eacute;sek</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Bőr&uuml;l&eacute;sek</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				&nbsp;</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Exkluz&iacute;v enteriőr (szorakoz&oacute;hely szerinti kialak&iacute;t&aacute;s)</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Elektromos tetőablak</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Elektromos tetőablak</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- K&uuml;lső dekor vil&aacute;g&iacute;t&aacute;s</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- K&uuml;lső dekor vil&aacute;g&iacute;t&aacute;s</strong></td>\r\n			<td style="border: 1px solid #424143;">\r\n				&nbsp;</td>\r\n			<td style="border: 1px solid #424143;">\r\n				<strong>- Exkluz&iacute;v enteriőr (szorakoz&oacute;hely szerinti kialak&iacute;t&aacute;s)</strong></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	<span style="font-size: 10pt;"><em><strong>Ital &eacute;s &eacute;tel d&iacute;jmentesen felhozhat&oacute; a partybuszra!</strong></em></span></p>\r\n<p>\r\n	<strong>Szemben a m&aacute;r megszokott limuzinokkal a Party Busban tal&aacute;lhat&oacute; t&aacute;nct&eacute;r, helyet biztos&iacute;t egy val&oacute;di bulinak. Itt mindenki megmutathatja, hogy milyen j&oacute;l &eacute;rzi mag&aacute;t bar&aacute;taival!</strong></p>\r\n<p>\r\n	<strong>Az ig&eacute;ny szerint rendelhető extra szolg&aacute;ltat&aacute;sokr&oacute;l m&aacute;r nem is besz&eacute;lve! &Uacute;gy, mit a Mixer, DJ , T&aacute;ncosl&aacute;ny/ Chippendale fi&uacute;. Ig&eacute;ny szerint az eg&eacute;sz este megszervez&eacute;s&eacute;ben is seg&iacute;ts&eacute;get ny&uacute;jtunk!</strong></p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(54, 'Galéria', 'galeria', '', 0, '', 0, 1, '', '', '', '', '', ''),
(55, 'Partybusz képek', 'partybusz_kepek', '', 54, '', 0, 1, '', '', '', '', '', ''),
(56, 'Limuzin képek', 'limuzin_kepek', '', 54, '', 1, 1, '', '', '', '', '', ''),
(57, 'Kínálatunk', 'kinalatunk', '<table align="center" border="0" cellpadding="5" cellspacing="5" padding="5px">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Partybuszos sz&uuml;linap</h1>\r\n				<p>\r\n					Lepd meg bar&aacute;taidat, csal&aacute;dtagjaidat sz&uuml;let&eacute;snapjukon egy exkluz&iacute;v partybuszos utaz&aacute;ssal, vagy ha te vagy az &uuml;nnepelt, aj&aacute;nd&eacute;kozd meg magadat &eacute;s a bar&aacute;ti k&ouml;r&ouml;det egy hatalmas nagy bulival!</p>\r\n				<p>\r\n					Megrendezheted sz&uuml;let&eacute;snapi bulidat a partybuszon a legjobb bar&aacute;taiddal, egy fergeteges &eacute;lm&eacute;nnyel egybek&ouml;tve! Erre az alkalomra a bulibusz lufikkal feld&iacute;sz&iacute;tve v&aacute;r titeket. Az &uuml;nnepl&eacute;s kezdődj&ouml;n ak&aacute;r a sz&uuml;let&eacute;snapos h&aacute;za előtt, majd folytat&oacute;djon egy csod&aacute;latos v&aacute;rosn&eacute;z&eacute;ssel &eacute;s egy felejthetetlen buliz&aacute;ssal, k&ouml;zben pedig koccintsatok j&eacute;gbe hűt&ouml;tt pezsgővel! Ez a sz&uuml;let&eacute;snap mindenki sz&aacute;m&aacute;ra &ouml;r&ouml;k eml&eacute;k marad. Vagy szervezz meglepet&eacute;sbulit annak, aki igaz&aacute;n fontos neked: a legjobb haverodnak, a gyerekkori bar&aacute;tnődnek, sz&uuml;leidnek, testv&eacute;reidnek! Az a legnagyobb kaland, ha az &uuml;nnepelt m&eacute;g csak nem is sejti, hogy a partybuszban, ami v&aacute;ratlanul leparkol a h&aacute;za előtt, a csal&aacute;dja &eacute;s a legjobb bar&aacute;tai v&aacute;rj&aacute;k koccint&aacute;sra k&eacute;szen!</p>\r\n			</td>\r\n			<td>\r\n				<p>\r\n					<img src="images/img/szulinap.jpg" /></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<p>\r\n					<img src="images/img/szalagavató.jpg" /></p>\r\n			</td>\r\n			<td>\r\n				<h1>\r\n					Partybusz szalagavat&oacute;ra</h1>\r\n				<p>\r\n					<strong>A szalagavat&oacute; a felnőttkor hat&aacute;ra. Olyan l&eacute;pcsőfok, amit illik m&eacute;lt&oacute;k&eacute;ppen meg&uuml;nnepelni, m&eacute;ghozz&aacute; egy &oacute;ri&aacute;si, őr&uuml;lt buliz&aacute;ssal!</strong></p>\r\n				<p>\r\n					A kisebbik partybuszba bef&eacute;r egy teljes &uuml;nneplő oszt&aacute;ly, vagy a nagyobbik buszba pedig ak&aacute;r az eg&eacute;sz buliz&oacute; &eacute;vfolyam &ndash; &iacute;gy a felfokozott hangulat&uacute; partyz&aacute;st m&aacute;r akkor elkezdhetitek, amikor a szalagavat&oacute; helysz&iacute;n&eacute;ről &aacute;tmentek az after party-ra. Alapozzatok vel&uuml;nk, &eacute;s mi garant&aacute;ljuk, hogy erre az est&eacute;re teljesen el fogj&aacute;tok felejteni a sulit, az oszt&aacute;lyzatokat, a kor&aacute;nkel&eacute;st &eacute;s a k&ouml;zelgő &eacute;retts&eacute;git! A partybusz b&eacute;rl&eacute;s ide&aacute;lis a v&eacute;gzős oszt&aacute;lyok r&eacute;sz&eacute;re &ndash; itt az ideje, hogy meg&eacute;rezz&eacute;tek azt a szabads&aacute;got, ami&eacute;rt igaz&aacute;n szuper &aacute;tl&eacute;pni a felnőttkor k&uuml;sz&ouml;b&eacute;t! Csapass&aacute;tok ezerrel!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Esk&uuml;vő partybusszal</h1>\r\n				<p>\r\n					<strong>Egy partybuszos esk&uuml;vő a n&aacute;szn&eacute;pnek is olyan eml&eacute;kezetes, mint a h&aacute;zasuland&oacute;knak!</strong></p>\r\n				<p>\r\n					A partybusz b&eacute;rl&eacute;s a n&aacute;szn&eacute;p sz&aacute;ll&iacute;t&aacute;s&aacute;nak a legide&aacute;lisabb m&oacute;dja, mert mire a vend&eacute;gsereg &aacute;t&eacute;r a templomb&oacute;l a vacsora helysz&iacute;n&eacute;re, m&aacute;r t&uacute;l van egy fergeteges k&ouml;z&ouml;s bulin, &eacute;s mindenki megismert mindenkit. &Eacute;s ami m&eacute;g enn&eacute;l is legfontosabb szempont: az &ouml;sszes megh&iacute;vott koccinthat az ifj&uacute; p&aacute;r boldogs&aacute;g&aacute;ra, nem kell foglalkozniuk a parkol&aacute;ssal, r&aacute;ad&aacute;sul egy maradand&oacute; &eacute;lm&eacute;nnyel is gazdagodtak. Term&eacute;szetesen a nagy napra mi is feld&iacute;sz&iacute;tj&uuml;k partybuszainkat szalagokkal, lufikkal, &eacute;s megegyez&eacute;s szerint j&eacute;gbe hűt&ouml;tt welcome italokkal valamint pezsgőkkel v&aacute;rjuk a vend&eacute;gsereget &ndash; hogy igaz&aacute;n &uuml;nnepi legyen a hangulat.</p>\r\n			</td>\r\n			<td>\r\n				<p>\r\n					<img alt="partybuszeskuvo" src="../images/img/partybuszmenyasszony.jpg" /></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<p>\r\n					<img alt="" src="../images/img/partybuszlegenybucsu.jpg" /></p>\r\n			</td>\r\n			<td>\r\n				<h1>\r\n					Partybusz leg&eacute;nyb&uacute;cs&uacute;ra</h1>\r\n				<p>\r\n					<strong>Az egyik bar&aacute;tod &eacute;let&eacute;ben k&ouml;zeledik az a nap, amitől m&eacute;g a legb&aacute;trabb f&eacute;rfinek is van egy kis gyomorg&ouml;rcse? P&aacute;r h&eacute;t, &eacute;s itt az esk&uuml;vő? Szervezzetek neki egy olyan leg&eacute;nyb&uacute;cs&uacute;t, amit soha nem felejt el!</strong></p>\r\n				<p>\r\n					A nagy nap előtt alaposan p&ouml;rgess&eacute;tek fel a hangulatot &eacute;s eressz&eacute;tek ki a gőzt! Legyen ez egy olyan alkalom, amikor hajnalig egy&uuml;tt buliztok, &eacute;s csak r&oacute;latok sz&oacute;l minden! Garant&aacute;ljuk, hogy a mi javaslatunk a legjobb az &ouml;sszes leg&eacute;nyb&uacute;cs&uacute; &ouml;tlet k&ouml;z&uuml;l! Mert mi m&aacute;s lenne alkalmasabb erre az alkalomra, mint a partybusz?! Lesz minden, amit szeretn&eacute;tek: j&oacute; kaja, bős&eacute;ges italv&aacute;laszt&eacute;k, eszm&eacute;letlen j&oacute; zene, helyes hostessl&aacute;nyok, &eacute;s&hellip;.az est&eacute;t sz&iacute;nes&iacute;theti k&eacute;r&eacute;sre ak&aacute;r Striptease show is, melynek megszervez&eacute;s&eacute;ben is sz&iacute;vesen seg&iacute;t&uuml;nk. ;)</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Le&aacute;nyb&uacute;cs&uacute; a partybuszon</h1>\r\n				<p>\r\n					<strong>Az ut&oacute;bbi &eacute;vekben a partybusz b&eacute;rl&eacute;s elengedhetetlen kell&eacute;ke lett a le&aacute;nyb&uacute;cs&uacute;knak! Ki mondta, hogy a l&aacute;nyok nem szeretik az eszm&eacute;letlen j&oacute; bulikat? Ők szeretik csak igaz&aacute;n!</strong></p>\r\n				<p>\r\n					Mi m&aacute;s lehetne t&ouml;k&eacute;letesebb meglepet&eacute;s a menyasszony sz&aacute;m&aacute;ra, mint mikor a legjobb bar&aacute;tnőivel megt&ouml;m&ouml;tt partybusz gurul a kapu el&eacute; hatalmas zen&eacute;vel, dud&aacute;l&aacute;ssal?&nbsp;</p>\r\n				<p>\r\n					A beerbike a sr&aacute;coknak val&oacute;, Neked a Partybusz!</p>\r\n				<p>\r\n					Hen Party programokkal, vicces feladatokkal.&nbsp;</p>\r\n				<p>\r\n					A programot sz&iacute;nes&iacute;theti welcome pezsgő &eacute;s a Chippendale show, hogy m&eacute;g jobban felp&ouml;rgess&uuml;k a hangulatot! T&ouml;bb sz&aacute;z le&aacute;nyb&uacute;cs&uacute;val a h&aacute;tunk m&ouml;g&ouml;tt garant&aacute;ljuk, hogy ez lesz az &uuml;nnepelt legeml&eacute;kezetesebb utols&oacute; fac&eacute;r napja! ;)&nbsp;</p>\r\n			</td>\r\n			<td>\r\n				<img alt="" src="../images/img/partybusz_leanybucsu.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<img src="images/img/cegesrendezveny.jpg" /></td>\r\n			<td>\r\n				<h1>\r\n					C&eacute;ges rendezv&eacute;nyek partybusszal</h1>\r\n				<p>\r\n					<strong>Szervezzen c&eacute;g&eacute;nek egy olyan partybuszos rendezv&eacute;nyt, amit a munkat&aacute;rsak m&eacute;g h&oacute;napokkal k&eacute;sőbb is j&oacute;kedvűen emlegetnek majd! A c&eacute;ges rendezv&eacute;nyeken ugyanis nem k&ouml;telező &aacute;m unatkozni, sőt&hellip;.egy partybuszos rendezv&eacute;nyen egyenesen lehetetlen!</strong></p>\r\n				<p>\r\n					A partybusz b&eacute;rl&eacute;s kiv&aacute;l&oacute; lehetős&eacute;geket ny&uacute;jt arra, hogy a munkat&aacute;rsak a megszokott munkak&ouml;rnyezet&uuml;kből kiszakadva egy j&oacute;val kreat&iacute;vabb, &ouml;szt&ouml;nzőbb, laz&aacute;bb helysz&iacute;nen is kipr&oacute;b&aacute;lj&aacute;k, milyen k&eacute;pess&eacute;gek rejlenek benn&uuml;k! Csapat&eacute;p&iacute;t&eacute;sre &eacute;s tr&eacute;ningekre egyar&aacute;nt alkalmas helysz&iacute;n a partybusz! &Eacute;s van m&eacute;g egy tipp&uuml;nk: a partybusz elsőrendű brainstorming (&ouml;tletb&ouml;rze) helysz&iacute;n is lehet, ahol a nyitottabb, laz&aacute;bb, kev&eacute;sb&eacute; form&aacute;lis helyzet seg&iacute;ti a r&eacute;sztvevőket az &bdquo;out of the box&rdquo; gondolkod&aacute;s megsz&uuml;let&eacute;s&eacute;ben. Ig&eacute;ny eset&eacute;n catering/bek&eacute;sz&iacute;t&eacute;s, hostessek, kivet&iacute;tők kezel&eacute;s&eacute;ben/megszervez&eacute;s&eacute;ben is seg&iacute;t&uuml;nk!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					C&eacute;ges buli</h1>\r\n				<p>\r\n					<strong>T&uacute;l sok volt mostan&aacute;ban a munka? Munkat&aacute;rsaival sikeresen v&eacute;ghezvittek egy nagyszab&aacute;s&uacute; projektet, de rengeteg stresszel &eacute;s t&uacute;l&oacute;r&aacute;val j&aacute;rt? R&aacute;f&eacute;rne mindenkire egy kis kikapcsol&oacute;d&aacute;s?</strong></p>\r\n				<p>\r\n					Minden c&eacute;gn&eacute;l megesik, hogy a rengeteg hat&aacute;ridős munka miatt h&oacute;napokon &aacute;t 150%-on kell teljes&iacute;teni. Tudjuk, hogy b&uuml;szke a csapat&aacute;ra, de vajon ezt ők is tudj&aacute;k? A k&ouml;sz&ouml;net&eacute;t nagyon egyszerűen kifejezheti: tegyen fel&eacute;j&uuml;k egy nagyvonal&uacute; gesztust, ami jelzi, hogy a c&eacute;gn&eacute;l senki sem csak egy l&aacute;thatatlan csavar a g&eacute;pezetben, hanem n&eacute;lk&uuml;l&ouml;zhetetlen csapattag. Rendezzen a csapat&aacute;nak egy rendhagy&oacute; bulit a partybuszra, &eacute;s ez a nap nem sz&oacute;lhat munk&aacute;r&oacute;l, csak a sz&oacute;rakoz&aacute;sr&oacute;l! Ismeri a mond&aacute;st: Work hard, play hard!</p>\r\n			</td>\r\n			<td>\r\n				<img src="images/img/cegesbuli.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<img src="images/img/partybussightseeing.jpg" /></td>\r\n			<td>\r\n				<h1>\r\n					V&aacute;rosn&eacute;z&eacute;s partybusszal:</h1>\r\n				<p>\r\n					<strong>A partybusz rendk&iacute;v&uuml;li &eacute;lm&eacute;nyt biztos&iacute;t a v&aacute;rosn&eacute;z&eacute;s alatt! Csak az indul&aacute;s helysz&iacute;n&eacute;t kell megadnod, &eacute;s mi gondoskodunk a tov&aacute;bbi l&aacute;tnival&oacute;kr&oacute;l.</strong></p>\r\n				<p>\r\n					V&aacute;rosn&eacute;z&eacute;s k&ouml;zben bar&aacute;taiddal &eacute;lvezhetitek a partybusz adta lehetős&eacute;geket, az eleg&aacute;ns bőr&uuml;l&eacute;seket, a k&eacute;nyelmes kanap&eacute;t, a komoly hangrendszert, a h&iacute;vogat&oacute; t&aacute;ncteret &eacute;s a j&eacute;gbe hűt&ouml;tt pezsgőket. Helyezz&eacute;tek magatokat k&eacute;nyelembe, &eacute;s &eacute;lvezz&eacute;tek a luxust, amit k&iacute;n&aacute;lunk nektek! Ez a partybuszos v&aacute;rosn&eacute;z&eacute;s mindenki sz&aacute;m&aacute;ra &ouml;r&ouml;k &eacute;lm&eacute;ny marad. &Eacute;rdemes &eacute;jszakai v&aacute;ltozatban is letesztelnetek a buszunkat, nincs szebb, mint a v&aacute;ros f&eacute;nyeit csod&aacute;lni egy poh&aacute;r j&eacute;ghideg pezsgőt kortyolgatva!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Partybuszos oszt&aacute;lytal&aacute;lkoz&oacute;</h1>\r\n				<p>\r\n					<strong>T&ouml;bb &eacute;ve nem tal&aacute;lkozt&aacute;l r&eacute;gi oszt&aacute;lyt&aacute;rsaiddal, de szeretn&eacute;d tudni, mi is t&ouml;rt&eacute;nt eddig vel&uuml;k, merre sodorta őket az &eacute;let? Rendezd meg az oszt&aacute;lytal&aacute;lkoz&oacute;tokat a partybuszra!</strong></p>\r\n				<p>\r\n					Hogy a hangulat igaz&aacute;n nosztalgikus legyen, hozhattok magatokkal r&eacute;gi k&eacute;peket, vide&oacute;kat, zen&eacute;ket, amiket lej&aacute;tszunk az utaz&aacute;s alatt, v&aacute;rosn&eacute;z&eacute;s k&ouml;zben pedig m&eacute;g t&aacute;ncolni &eacute;s bulizni is tudtok az erre kialak&iacute;tott t&aacute;nct&eacute;ren! Lehet, hogy kamaszk&eacute;nt nem merted soha felk&eacute;rni az oszt&aacute;ly legjobb csaj&aacute;t, de most elj&ouml;tt a te időd! Mi&eacute;rt hagyn&aacute;d ki az alkalmat? Nem veszthetsz semmit! Ja, &eacute;s v&eacute;gre &oacute;r&aacute;kig dum&aacute;lhatsz a r&eacute;gi haverjaiddal, r&aacute;&eacute;rősen poharazgatva, an&eacute;lk&uuml;l, hogy azon agg&oacute;dn&aacute;l, ki fog vezetni ut&aacute;na. &Eacute;lvezd ezt a k&uuml;l&ouml;nleges időutaz&aacute;st, &eacute;s garant&aacute;ljuk, a r&eacute;gi oszt&aacute;lyod minden tagja h&aacute;l&aacute;s lesz neked!</p>\r\n			</td>\r\n			<td>\r\n				<img src="images/img/osztalytalalkozo.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<img src="images/img/filmforgatas.jpg" /></td>\r\n			<td>\r\n				<h1>\r\n					Filmforgat&aacute;s a partybuszon</h1>\r\n				<p>\r\n					Hi&aacute;nyzik valami k&uuml;l&ouml;nleges helysz&iacute;n a most k&eacute;sz&uuml;lő videoklipedbe, filmedbe? A partybusz b&eacute;rl&eacute;s t&ouml;k&eacute;letes megold&aacute;s a sz&aacute;modra! Hogy mit aj&aacute;nlunk? Gy&ouml;ny&ouml;rű bőr&uuml;l&eacute;seket, komoly hangrendszert, hangvez&eacute;relt f&eacute;nyeket, t&aacute;ncteret, mindent, ami csak kell a t&ouml;k&eacute;letes jelenetekhez! Forgat&aacute;s ak&aacute;r a partybusz tetej&eacute;n! Ig&eacute;ny eset&eacute;n t&aacute;nccsapatot biztos&iacute;tunk a forgat&aacute;shoz! C&eacute;lunk az, hogy professzion&aacute;lis felt&eacute;teleket biztos&iacute;tsunk projektednek: ak&aacute;rmilyen őr&uuml;lt &ouml;tleted is t&aacute;mad, seg&iacute;t&uuml;nk megval&oacute;s&iacute;tani. A partybuszos filmforgat&aacute;s olyan lenyűg&ouml;ző &eacute;lm&eacute;ny lesz, amire majd 100 &eacute;ves korodban is eml&eacute;kszel! K&eacute;szen &aacute;llsz? Csap&oacute;, indul!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Partybuszos randev&uacute;</h1>\r\n				<p>\r\n					<strong>Megtal&aacute;ltad &eacute;leted p&aacute;rj&aacute;t? Szervezz kettőt&ouml;knek egy felejthetetlen randev&uacute;t egy partybuszban!</strong></p>\r\n				<p>\r\n					Ha valaki megtal&aacute;lja az igazit, azt mindenk&eacute;ppen m&eacute;lt&oacute;k&eacute;ppen meg kell &uuml;nnepelni! Biztosan nem &aacute;tlagos nő az, akibe beleszerett&eacute;l, &eacute;s ki tudja, h&aacute;nyan v&aacute;gyakoznak m&eacute;g ut&aacute;na. Ha t&eacute;nyleg rabul ejtett t&eacute;ged, meg kell mutatnod, hogy te vagy sz&aacute;m&aacute;ra a nagy Ő! Var&aacute;zsold el, nyűg&ouml;zd le, k&aacute;pr&aacute;ztasd el kedvesedet, &eacute;s vallj neki szerelmet egyed&uuml;l&aacute;ll&oacute;an romantikusan &ndash; egy &eacute;rz&eacute;ki, &eacute;jszakai partybuszos randev&uacute;val! Erre az alkalomra k&uuml;l&ouml;nleges dekor&aacute;ci&oacute;t kap a busz, hogy m&eacute;g t&ouml;k&eacute;letesebb legyen a tal&aacute;lkoz&oacute;, &iacute;gy k&uuml;l&ouml;nleges k&ouml;rnyezetben, meghitt hangulatban gy&ouml;ny&ouml;rk&ouml;dhettek az &eacute;jszakai v&aacute;ros f&eacute;nyeiben.</p>\r\n			</td>\r\n			<td>\r\n				<img src="images/img/randevu.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<img src="images/img/Barhopping.jpg" /></td>\r\n			<td>\r\n				<h1>\r\n					Bar hopping partybusszal</h1>\r\n				<p>\r\n					J&aacute;rj legjobb bar&aacute;taiddal kocsm&aacute;r&oacute;l kocsm&aacute;ra a belv&aacute;rosban, &eacute;s a partybuszos utaz&aacute;s alatt is igyatok n&eacute;h&aacute;ny italt! Fogadjunk, hogy aznap este senki nem marad szomjas!</p>\r\n				<p>\r\n					Ha &uacute;gy &eacute;rzed, itt az ideje egy igaz&aacute;n z&uacute;z&oacute;s est&eacute;nek, &eacute;s szeretn&eacute;l egy kocsmat&uacute;r&aacute;t a haverokkal, t&aacute;rt karokkal v&aacute;runk! De előre sz&oacute;lunk: a partybuszos bar hopping csak igaz&aacute;n rutinos buliz&oacute;knak val&oacute;! Ha k&eacute;szen &aacute;lltok egy igaz&aacute;n eszement, felejthetetlen kocsmat&uacute;r&aacute;ra, b&eacute;reljetek partybuszt, &eacute;s mi megmutatjuk, milyen a v&aacute;ros buliz&oacute;s, &eacute;jszakai arca! Olyan lesz, mintha egy csom&oacute; őr&uuml;lt h&aacute;zibulin z&uacute;zn&aacute;tok v&eacute;gig egyetlen este! Csak az a k&eacute;rd&eacute;s: b&iacute;rni fogj&aacute;tok hajnalig? Ha nem akarsz tekerni a s&ouml;rbiciklin / beer-bike akkor gyere vel&uuml;nk &eacute;s csak sz&oacute;rakozz!</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<h1>\r\n					Partybuszos gyerek party</h1>\r\n				<p>\r\n					&nbsp;</p>\r\n				<p>\r\n					<span style="color: #ffffff;"><span style="font-size: 12.8px;">Ha esetlegesen kifogytunk m&aacute;r az &ouml;tletekből &eacute;s nem jut esz&uuml;nkbe ann&aacute;l jobb megold&aacute;s, hogy j&aacute;tsz&oacute;h&aacute;zba vagy McDondals-ba vigy&uuml;k gyermekeinket &eacute;s oda szervezz&uuml;k a sz&uuml;linapi bulit, akkor mi tudjuk a megold&aacute;st! ;) A partybusz a gyerekek sz&aacute;m&aacute;ra egy feledhetetlen &eacute;lm&eacute;nyt fog biztos&iacute;tani, hiszen ha belegondolunk, mindig is vag&aacute;nyak akartunk lenni &eacute;s kipr&oacute;b&aacute;lni azt amit a nagyok is...! </span><br />\r\n					<span style="font-size: 12.8px;">- A&nbsp;zs&uacute;r sz&oacute;t el lehet felejteni, egy ilyen komoly partyn&aacute;l!&nbsp;</span><br />\r\n					<span style="font-size: 12.8px;">Kiv&aacute;l&oacute; helysz&iacute;n a Partybusz ezekre az alkalmakra, mert a gyerekek egy&uuml;tt t&aacute;ncolhatnak, ehetnek, ihatnak sőt&nbsp;fel&uuml;gyeltet&nbsp;alatt vannak!</span><br />\r\n					<span style="font-size: 12.8px;">Sz&aacute;mtalan alkalommal siker&uuml;lt m&aacute;r &aacute;t&eacute;ln&uuml;nk ezeket, az ig&eacute;nyes, odaad&oacute; sz&uuml;lők &aacute;ltal megszervezett bulikat! </span></span></p>\r\n				<p>\r\n					<span style="color: #ffffff;">El&aacute;rulunk egy nagy titkot is, hogy&nbsp;sokszor&nbsp;a&nbsp;gyerekek jobban beringatj&aacute;k a partybusz egy ilyen Tinibuli alkalm&aacute;val mint a felnőttek... ;)<br />\r\n					Ezek ut&aacute;n gondolhatjuk, hogy nem fog unalmasan telni ez a feledhetetlen alkalom.</span></p>\r\n			</td>\r\n			<td>\r\n				<img src="images/img/partybuszgyerekek.jpg" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n				<img src="images/img/20-fos-partybusz.jpg" /></td>\r\n			<td>\r\n				<h1>Reptéri Transfer</h1><p>Reptéri transzfer\r\n \r\nTranszfer partybusszal? Ráadásul fejenként egy taxi árán?\r\n \r\nMiért ne?! Tegyétek felejthetetlenné Budapestre érkezésetek egy jó kis partybuszozással a belváros felé !Legújabb Limo-transzfer buszunk exkluzív kialakítása lehetővé teszi, hogy hazaút közben táncoljatok,  élvezzétek a bőrülések kényelmét és napjaink legbulisabb slágereit! Legénybúcsú esetén Striptease táncost is tudunk biztosítani, valamint kedvenc italjaitok beszerzésével sem kell foglalkoznotok!Eközben a csomagok biztonságban pihennek az erre kialakított ülések alatti boxokban...\r\n \r\nHatalmas előnye, hogy rögtön a kijáratnál vesszük fel utasainkat! ;)\r\n </p></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(58, 'Áraink', 'araink', '', 0, '', 0, 1, '', '', '', '', '', ''),
(59, 'Rendelés', 'rendeles', '', 0, '', 0, 1, '', '', '', '', '', ''),
(60, 'Kapcsolat', 'kapcsolat', '', 0, '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 0, 'Partybuszok és limuzinok rendelése', 'cf0cf-slider1.png', '', 'Partybusz.co.hu', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `termekek`
--

INSERT INTO `termekek` (`id`, `nev`, `url`, `kategoria`, `lead`, `leiras`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`, `statusz`, `gyarto`, `marka`, `kiemelt`, `ar`, `ar_akcios`, `valuta`, `ar_beszerzesi`, `suly`, `raktarkeszlet`) VALUES
(1, '20 személyes Partybusz ', '', 3, '', '<ul class="pricing-content list-unstyled">\r\n	<li>\r\n		1 &oacute;ra Partybuszoz&aacute;s 55.000 Ft</li>\r\n	<li>\r\n		2 &oacute;ra Partybuszoz&aacute;s 75.000 Ft</li>\r\n	<li>\r\n		3 &oacute;ra Partybuszoz&aacute;s 95.000 Ft</li>\r\n	<li>\r\n		Minden tov&aacute;bbi f&eacute;l &oacute;ra: 10.000 Ft</li>\r\n	<li>\r\n		&Iacute;gy 20 fő eset&eacute;n 2.750 Ft/fő/1&oacute;ra</li>\r\n</ul>\r\n', '9877a-20-fos-partybusz.jpg', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0),
(2, '8 személyes Lincoln limuzinok', '', 4, '', '<ul class="pricing-content list-unstyled">\r\n	<li>\r\n		1 &oacute;ra 30.000 Ft</li>\r\n	<li>\r\n		2 &oacute;ra 45.000 Ft</li>\r\n	<li>\r\n		3 &oacute;ra 60.000 Ft</li>\r\n	<li>\r\n		Minden tov&aacute;bbi &oacute;ra: 7.500 Ft</li>\r\n	<li>\r\n		Minden tov&aacute;bbi f&eacute;l &oacute;ra: 15.000 Ft</li>\r\n</ul>\r\n', '35e97-_mg_2185.jpg', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
