<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oldal extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


        $this->load->model("Lekerdezes");
        $this->load->model("Alapfunction");
        $this->load->helper("url");
        $this->load->library("pagination");

    }

    public function index()
    {		
			$nyelv = $this->session->userdata('site_lang');
			$data['nyelv'] = $nyelv;
			$data['slider'] = $this->db->query("SELECT * FROM slider");
            $data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$data['oldal'] = $this->Lekerdezes->oldal('fooldal');
			$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
			$data['galeria'] = $this->db->query('SELECT * FROM galeria ORDER BY id DESC limit 0,6');
            $this->load->view("site/fooldal",$data);
    }
	public function jajne()
    {		
			$nyelv = $this->session->userdata('site_lang');
			$data['nyelv'] = $nyelv;
            $data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$data['oldal'] = $this->Lekerdezes->oldal('fooldal');
			$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
            $this->load->view("site/jajne",$data);
    }


    public function url()
    {
        $nyelv = $this->input->get('lang', TRUE);
		
	$url = $this->uri->segment(1);
        $data['oldal'] = $this->Lekerdezes->oldal($url);
        $nyelv = $this->session->userdata('site_lang');
		$data['nyelv'] = $nyelv;
        $data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['oldal'] = $this->Lekerdezes->oldal($url);
		$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
        $this->load->view("site/oldal",$data);

	}
	
	public function specoldal()
    {
		$url = $this->uri->segment(1);
        $data['oldal'] = $this->Lekerdezes->oldal($url);
        $nyelv = $this->session->userdata('site_lang');
		$data['nyelv'] = $nyelv;
        $data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['oldal'] = $this->Lekerdezes->oldal($url);
		$data['primari'] = $this->Lekerdezes->primari($data['nyelv']);
		$data['arak_busz'] = $this->db->query('SELECT * FROM termekek WHERE kategoria=3');
		$data['arak_lim'] = $this->db->query('SELECT * FROM termekek WHERE kategoria=4');
		if($url == 'partybusz_kepek' || $url == 'limuzin_kepek'){
			if($url == 'partybusz_kepek'){
				$data['galeria'] = $this->db->query('SELECT * FROM galeria WHERE nev="partybusz"');
			}else{
				$data['galeria'] = $this->db->query('SELECT * FROM galeria WHERE nev="limuzin"');
			}
			$this->load->view("site/galeria",$data);
		}else{			
			$this->load->view("site/".$url,$data);
		}
	}
	public function sendmail()
     {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
			$subject = $this->input->post("subject");
            $message = $this->input->post("message");          
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject("Partybusz.co.hu | ".$subject);
            $this->email->message($message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
	 public function rendeles()
	 {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
			$subject = $this->input->post("subject");
            $message = $this->input->post("message"); 
			$phone = $this->input->post("phone"); 
			$date = $this->input->post("date"); 
			$place = $this->input->post("place");
			$jarmu = $this->input->post("jarmu");
			$extra = $this->input->post("extra");  			
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject("Partybusz.co.hu | ".$subject);
            $this->email->message("Név: ".$senderName.", Telefonszám: ".$phone.", Dátum: ".$date.", Honnan-hova: ".$place.", Jármű: ".$jarmu.", Extrák: ".$extra.", Egyéb igények/kérdések: ".$message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
}