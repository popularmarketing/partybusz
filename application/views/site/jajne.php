<!DOCTYPE html>
<!-- 
Template Name: RockOn HTML Template
Version: 1.1
Author: Kamleshyadav
Website: http://himanshusofttech.com/
Purchase: http://themeforest.net/user/kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from kamleshyadav.com/rockon/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Apr 2016 10:05:12 GMT -->
<head>
<meta charset="utf-8" />
<title><?php echo $beallitasok->oldalnev?> | 404 Hiba</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description"  content="<?php echo $beallitasok->fooldal_description?>"/>
<meta name="keywords" content="<?php echo $beallitasok->fooldal_keywords?>">
<meta name="author"  content="PopularMarketing"/>
<meta name="MobileOptimized" content="320">
<!--srart theme style -->
<link href="<?php echo base_url();?>/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/ImageGrid/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/datetime/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/video/mediaelementplayer.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>
<link rel="stylesheet" id="theme-pattern" type="text/css" href="#"/>
<!-- end theme style -->
<link rel="shortcut icon" href="favicon.png" />
</head>
<!--body start-->
<body>
<!--header start-->
<header id="rock_header_otherpage">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="rock_logo"> <a href="fooldal"><img src="assets/uploads/files/<?php echo $beallitasok->logo?>" alt="logo"  /></a> </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <div class="rock_menu_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu <i class="fa fa-bars"></i></div>
          <div class="rock_menu">
            <ul class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php foreach ($primari as $row) {
                                $url = base_url("" . $row['url']);
                                if (empty($row['gyerekek'])) {
                                    ?>
                                    <li><a href="<?php echo $url ?>"><?php echo $row['nev'] ?></a></li>                                      
            <?php } else { ?>
              <li ><a><?php echo $row['nev'] ?></a> 
                <ul class="sub-menu">
					<?php foreach($row['gyerekek'] as $gyRow){
						$urlChild = base_url("" . $gyRow['url']);
					?>
					<li><a href="<?php echo $urlChild ?>"><?php echo $gyRow['nev'] ?></a></li>
				  <?php }?>
                </ul>
				</li>
				<?php }?>
				<?php }?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!--header end--> 
<!--page title start-->
<div class="rock_page_title_main">
  <div class="rock_page_title_bg">
    <section class="main">
      <div id="rock_page_title_bg" class="ri-grid ri-grid-size-3">
        <ul>
          <li><a href="#"><img src="images/medium/1.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/2.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/3.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/4.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/5.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/6.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/7.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/8.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/9.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/10.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/11.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/12.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/13.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/14.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/15.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/16.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/17.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/18.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/19.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/20.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/21.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/22.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/23.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/24.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/25.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/26.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/27.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/28.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/29.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/30.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/31.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/32.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/33.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/34.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/35.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/36.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/37.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/38.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/39.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/40.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/41.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/42.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/43.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/44.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/45.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/46.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/47.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/48.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/49.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/50.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/51.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/52.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/53.jpg" alt="" /></a></li>
          <li><a href="#"><img src="images/medium/54.jpg" alt="" /></a></li>
        </ul>
      </div>
    </section>
  </div>
  <div class="rock_page_title">
    <div class="container">
      <div class="rock_heading_div">
        <div class="rock_heading">
          <h1>404 Hiba</h1>
          <p>X</p>
        </div>
      </div>
      <div class="rock_pager">
        <ul>
          <li><a href="fooldal">Főoldal</a></li>
          <li><a href="#">404 Hiba</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--page title end-->
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
      <div class="rock_404">
        <p>Hmmmm... Valami nincs rendben!</p>
        <div>
          <h1>404</h1>
          <h5>hiba</h5>
        </div>
        <p>Mindenhol megnéztük, de sajnos a keresett oldalt nem találjuk. Menjen vissza a <a href="index.html">Főoldalra</a> vagy egy előző oldalra. 
      </div>
    </div>
  </div>
  <div class="row">
    <div class="rock_404_search">
      <form class="form-inline" role="form">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Search here for other page" id="inputSuccess4">
          <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
      </form>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<?php include('footer.php');?>