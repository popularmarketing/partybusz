<?php include('header.php');?>
<?php include('primari.php');?>
<div class="clearfix"></div>
<div class="container">
  <div class="rock_contact">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <form role="form" action="oldal/sendmail" method="post">
          <div class="form-group">
            <input type="text" class="form-control" id="uname" name="senderName" placeholder="Név">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="uemail" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="web_site" name="subject" placeholder="Tárgy">
          </div>
          <div class="form-group">
            <textarea rows="10" class="form-control" id="message" name="message" placeholder="Üzenet"></textarea>
          </div>
          <input type="submit" id="em_sub" class="btn btn-default btn-lg" value="Üzenet Küldése">
        </form>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="rock_contact_detail">
          <div>
            <p><i class="fa fa-envelope"></i> <a href="#"><?php echo $beallitasok->nyilvanosemail?></a></p>
            <p><i class="fa fa-mobile"></i> <a href="#"><?php echo $beallitasok->mobil?></a></p>
          </div>
          <div>
            <p><i class="fa fa-map-marker"></i> <a href="#"><?php echo $beallitasok->uzletcim?></a></p>
            <p><i class="fa fa-globe"></i> <a href="#"><?php echo base_url();?></a></p>
          </div>
        </div>
        <div class="rock_map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21559.228535627153!2d19.074642384840146!3d47.51126920461877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc99715d96fb%3A0xb00c428ea695423!2sBudapest%2C+1146!5e0!3m2!1shu!2shu!4v1460921731637" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php');?>