<!--header start-->
<!--SAAAJT-->
<header id="rock_header">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="rock_logo"> <a href="index.html"><img src="assets/uploads/files/<?php echo $beallitasok->logo?>" alt="logo"  /></a> </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <div class="rock_menu_toggle navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">Menu <i class="fa fa-bars"></i></div>
          <div class="rock_menu">
          <ul class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php foreach ($primari as $row) {
                                $url = base_url("" . $row['url']);
                                if (empty($row['gyerekek'])) {
                                    ?>
                                    <li><a href="<?php echo $url ?>"><?php echo $row['nev'] ?></a></li>                                      
            <?php } else { ?>
              <li ><a><?php echo $row['nev'] ?></a> 
                <ul class="sub-menu">
					<?php foreach($row['gyerekek'] as $gyRow){
						$urlChild = base_url("" . $gyRow['url']);
					?>
					<li><a href="<?php echo $urlChild ?>"><?php echo $gyRow['nev'] ?></a></li>
				  <?php }?>
                </ul>
				</li>
				<?php }?>
				<?php }?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!--header end--> 