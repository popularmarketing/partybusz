<div class="clearfix"></div>
<div class="rock_copyright">
      <div class="rock_copyright_bg">
        <section class="main">
          <div id="ri-grid2" class="ri-grid ri-grid-size-3">
            <ul>
              <li><a href="#"><img src="images/medium/1.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/2.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/3.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/4.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/5.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/6.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/7.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/8.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/9.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/10.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/11.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/12.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/13.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/14.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/15.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/16.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/17.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/18.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/19.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/20.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/21.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/22.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/23.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/24.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/25.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/26.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/27.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/28.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/29.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/30.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/31.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/32.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/33.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/34.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/35.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/36.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/37.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/38.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/39.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/40.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/41.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/42.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/43.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/44.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/45.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/46.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/47.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/48.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/49.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/50.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/51.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/52.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/53.jpg" alt="" /></a></li>
              <li><a href="#"><img src="images/medium/54.jpg" alt="" /></a></li>
            </ul>
          </div>
        </section>
      </div>
      <div class="rock_copyright_div">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
              <p>@ 2016 Minden jog fenntartva</p>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
              <div class="rock_social">
                <ul>
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-5 col-md-8 col-sm-12 col-lg-offset-0 col-md-offset-2 col-sm-offset-0">
              <p><a href="http://popularmarketing.hu" target="_blank">Weboldal készítés <img src="img/logo.png" alt="PopularMarketing" height="50px"></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--main container end--> 
<!--main javascript start--> 
<script type="text/javascript" src="/js/jquery-1.11.1.js"></script> 

<!--slider background javascript--> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/ImageGrid/js/modernizr.custom.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/ImageGrid/js/jquery.gridrotator.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/owl-carousel/owl.carousel.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/bootstrap-select.js"></script> 

<!--player--> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/player/js/mediaelement-and-player.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/easing/jquery.easing.1.3.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/bxslider/jquery.bxslider.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/fancybox/jquery.fancybox.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/smoothscroll/smoothscroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/single-page/single-0.1.0.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>/js/custom.js"></script> 

<!--datetime picker--> 
<script type="text/javascript" src="<?php echo base_url();?>/js/plugin/datetime/jquery.datetimepicker.js"></script> 
<!--main javascript end-->
</body>
<!--body end-->

<!-- Mirrored from kamleshyadav.com/rockon/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Apr 2016 10:08:00 GMT -->
</html>