<!DOCTYPE html>
<!-- 
Template Name: RockOn HTML Template
Version: 1.1
Author: Kamleshyadav
Website: http://himanshusofttech.com/
Purchase: http://themeforest.net/user/kamleshyadav
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<!-- BEGIN HEAD -->

<!-- Mirrored from kamleshyadav.com/rockon/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Apr 2016 10:05:12 GMT -->
<head>
<meta charset="utf-8" />
<title><?php echo $beallitasok->oldalnev?> | <?php echo $oldal->nev?></title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description"  content="<?php echo $beallitasok->fooldal_description?>"/>
<meta name="keywords" content="<?php echo $beallitasok->fooldal_keywords?>">
<meta name="author"  content="PopularMarketing"/>
<meta name="MobileOptimized" content="320">
<!--srart theme style -->
<link href="<?php echo base_url();?>/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/ImageGrid/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/datetime/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>/js/plugin/video/mediaelementplayer.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" id="theme-color" type="text/css" href="#"/>
<link rel="stylesheet" id="theme-pattern" type="text/css" href="#"/>
<!-- end theme style -->
<link rel="shortcut icon" href="favicon.png" />
</head>
<!--body start-->
<body>