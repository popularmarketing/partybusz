<?php include('header.php');?>
<?php include('primari.php');?>
<!--page title end-->
<div class="clearfix"></div>
<div class="container">
  <div class="rock_main_gallery">
    <div class="main_gallery">
      <div id="photo_tab" class="main_gallery_tab_content animated fadeInDown">
	  
		<?php $count=1; foreach($galeria->result() as $row){
				if($count == 1 || ($count-1)%3 == 0){
				?>
        <div class="row">
				<?php }?>
					<a class="fancybox" data-fancybox-group="group1" href="assets/uploads/galeria/<?php echo $row->file?>">
					  <div class="col-lg-4 col-md-4 col-sm-4 main_gallery_item">
						<div class="rock_club_photo_slider_item">
						  <div class="rock_club_photo_item"> <img src="assets/uploads/galeria/<?php echo $row->file?>" alt="" />
							<div class="rock_club_photo_overlay">
							  <div class="photo_link animated fadeInDown"></div>
							</div>
						  </div>
						</div>
					  </div>
					  </a>
		<?php if($count%3 == 0){?>
        </div>
		<?php } $count++; }?>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php');?>