<?php include('header.php');?>
<?php include('primari.php');?>
<div class="clearfix"></div>
<div class="container">
  <div class="row">
    <div class="table_form">
      <form role="form" action="oldal/rendeles" method="post">
		<div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control" id="booking_fname" name="senderName" placeholder="Teljes Név">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control" id="booking_table_no" name="subject" placeholder="Rendelés / Ajánlatkérés">
          </div>
        </div>
		<div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control" id="booking_lname" name="email" placeholder="Email">
          </div>
        </div>
		<div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control" id="booking_lname" name="phone" placeholder="Telefonszám">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control booking_date" class="" id="datetimepicker" name="date" placeholder="Mikor">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <div class="form-group">
            <input type="text" class="form-control" id="booking_guest" name="place" placeholder="Honnan - Hová">
          </div>
        </div>
		<div class="col-lg-12 col-md-12 col-sm-12">
          <div class="row">
			<div class="col-md-6">
				<input type="radio" name="jarmu" value ="Partybusz 20 főig">Partybusz 20 főig</br>
				<input type="radio" name="jarmu" value ="Partybusz 35 főig">Partybusz 35 főig</br>
				<input type="radio" name="jarmu" value ="Partybusz 50 főig">Partybusz 50 főig</br>
				<input type="radio" name="jarmu" value ="Partybusz 70 főig">Partybusz 70 főig</br>
				<input type="radio" name="jarmu" value ="Lincoln 8 főig">Lincoln 8 főig</br>
				<input type="radio" name="jarmu" value ="Lincoln Navigator 14 főig">Lincoln Navigator 14 főig</br>
				<input type="radio" name="jarmu" value ="Hummer H2 black 16 főig">Hummer H2 black 16 főig</br>
				<input type="radio" name="jarmu" value ="Hummer H2 white 16 főig">Hummer H2 white 16 főig</br>
				<input type="radio" name="jarmu" value ="Hummer H2 XL 18 főig">Hummer H2 XL 18 főig</br>
			</div>
			<div class="col-md-6">
				<input type="checkbox" name="extrak" value="Welcome 4 üveg pezsgő"> Welcome 4 üveg pezsgő (5.000 Ft)<br>
				<input type="checkbox" name="extrak" value="Welcome 8 üveg pezsgő"> Welcome 8 üveg pezsgő (10.000 Ft)<br>
				<input type="checkbox" name="extrak" value="Busz díszítése lufikkal"> Busz díszítése lufikkal (4.000Ft/10db)<br>
				<input type="checkbox" name="extrak" value="Sztriptíz show"> Sztriptíz show (25.000Ft/Lány/15perc show)<br>
				<input type="checkbox" name="extrak" value="Chippendale show"> Chippendale show (25.000Ft/Fiú/15perc show)<br>
				<input type="checkbox" name="extrak" value="Budapest határán kívüli kiszállás/végzés"> Budapest határán kívüli kiszállás/végzés (~500 Ft/km + esetleges egyéb költségek)<br>
				<input type="checkbox" name="extrak" value="Party fotós"> Party fotós (15.000 Ft/~30 perc)<br>
			</div>
		  </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="form-group">
            <textarea rows="5" class="form-control" id="booking_instruction" name="message" placeholder="Egyéb igények/kérdések"></textarea>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <input type="submit" id="booking_sub" class="btn btn-default btn-lg pull-right" value="Küldés">
          <p id="booking_err"></p>
        </div>
      </form>
    </div>
  </div>
</div>
<?php include('footer.php');?>