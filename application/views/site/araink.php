<?php include('header.php');?>
<?php include('primari.php');?>
<div class="clearfix"></div>
<div class="rock_price">
  <div class="container">
	<h1>Partybuszok</h1>
	<?php $count=1; foreach($arak_busz->result() as $row){
		if($count==1||($count-1)%3==0){
		?>
    <div class="row">
      <div class="col-lg-12">
	  <?php }?>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="pricing">
            <div class="pricing-head">
              <h3><?php echo $row->nev?></h3>
			  <h4><img src="assets/uploads/termekek/<?php echo $row->fokep?>" style="max-width:200px"></h4>
            </div>
            <?php print_r($row->leiras);?>
            <div class="pricing-footer">
              <a href="rendeles" class="btn btn-default">Megrendelés</a> </div>
          </div>
        </div>
		<?php if($count%3==0){?>
      </div>
    </div>
	<?php } $count++; }?>
  </div>
  <div class="container">
	<h1>Limuzinok</h1>
	<?php $count=1; foreach($arak_lim->result() as $row){
		if($count==1||($count-1)%3==0){
		?>
    <div class="row">
      <div class="col-lg-12">
	  <?php }?>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="pricing">
            <div class="pricing-head">
              <h3><?php echo $row->nev?></h3>
			  <h4><img src="assets/uploads/termekek/<?php echo $row->fokep?>" style="max-width:200px"></h4>
            </div>
            <?php print_r($row->leiras);?>
            <div class="pricing-footer">
              <a href="rendeles" class="btn btn-default">Megrendelés</a> </div>
          </div>
        </div>
		<?php if($count%3==0){?>
      </div>
    </div>
	<?php } $count++; }?>
  </div>
</div>
<?php include('footer.php');?>