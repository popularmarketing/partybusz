<!--slider start-->
<div class="rock_slider_div">
  <section class="main">
    <div id="ri-grid" class="ri-grid ri-grid-size-3">
      <ul>  <!--Minimum 33 images required in this Rotating grid-->
        <li><a href="#"><img src="images/medium/1.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/2.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/3.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/4.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/5.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/6.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/7.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/8.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/9.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/10.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/11.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/12.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/13.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/14.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/15.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/16.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/17.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/18.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/19.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/20.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/21.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/22.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/23.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/24.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/25.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/26.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/27.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/28.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/29.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/30.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/31.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/32.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/33.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/34.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/35.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/36.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/37.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/38.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/39.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/40.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/41.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/42.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/43.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/44.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/45.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/46.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/47.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/48.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/49.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/50.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/51.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/52.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/53.jpg" alt="" /></a></li>
        <li><a href="#"><img src="images/medium/54.jpg" alt="" /></a></li>
      </ul>
    </div>
  </section>
  <div class="rock_slider">
    <div id="carousel" class="carousel slide carousel-fade">
		<ol class="carousel-indicators">
			<?php $count=0; foreach($slider->result() as $row){?>
				<li data-target="#carousel" data-slide-to="<?php echo $count?>" <?php if($count==0){?>class="active"<?php }?>></li>
			<?php $count++; }?>
		</ol>
	  <?php foreach($slider->result() as $row){?>
      <div class="carousel-inner">
        <div class="active item"> <img class="animated bounceInLeft" src="assets/uploads/slider/<?php echo $row->file?>" alt="" />
          <div class="carousel-caption">
            <div class="rock_logo_slider"> <a href="fooldal"><img class="animated bounceIn" src="assets/uploads/files/<?php echo $beallitasok->favicon?>" alt="" /></a> </div>
            <div class="rock_slider_content"> <a class="btn btn-default btn-lg rock_slider_btn animated fadeInDown"><?php echo $row->url?></a>
              <p class="animated fadeInDown"><?php echo $row->nev?></p>
            </div>
          </div>
        </div>
      </div>
	  <?php }?>
    </div>
  </div>
</div>
<!--slider end--> 